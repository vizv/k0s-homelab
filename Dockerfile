FROM docker.io/library/alpine:3.15.0

RUN apk add --no-cache tini
COPY /prebuild/k0s /usr/local/bin/

ENV CONTAINER_RUNTIME_ENDPOINT=unix:///run/k0s/containerd.sock \
    KUBECONFIG=/var/lib/k0s/pki/admin.conf

COPY /entrypoint.sh /
COPY /crds/ /crds/

ENTRYPOINT ["/sbin/tini", "--", "/bin/sh", "/entrypoint.sh"]
CMD ["k0s", "controller", "--config=/etc/k0s/config.yaml", "--enable-worker", "--no-taints"]

COPY /config-template.yaml /config-template.yaml
