#!/bin/sh -ex

render() { sh -c "echo \"$(cat "$1" | sed 's/["\]/\\&/g')\""; }

mkdir -p /etc/k0s
render /config-template.yaml >/etc/k0s/config.yaml

mkdir -p /var/lib/k0s/manifests/
mv /crds /var/lib/k0s/manifests/

[ -z "$@" ] && exec /bin/sh
exec "$@"
