.PHONY: up down reset logs watch-logs watch-pod

build:
	docker build --pull -t "k0s-homelab:test" .

up: build
	docker-compose up -d -t0

down:
	docker-compose down -t0

reset: down
	sudo rm -rf tmp

logs:
	docker-compose logs

watch-logs:
	docker-compose logs -f

watch-pods:
	@docker-compose exec k0s cat /var/lib/k0s/pki/admin.conf > ~/.kube/config
	kubectl get pods -Aw
watch-charts:
	@docker-compose exec k0s cat /var/lib/k0s/pki/admin.conf > ~/.kube/config
	kubectl get charts -Aw
