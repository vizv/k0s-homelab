* Fix cert-manager `Waiting for dns-01 challenge propagation: DNS record for "..." not yet propagated` error
* Add external-dns
* Improve MinIO probes
* customize MinIO Docker image
* Add https://kubernetes.github.io/ingress-nginx/examples/auth/oauth-external-auth/
* Add Prometheus Alertmanager plugin for Grafana
* Add https://grafana.com/grafana/dashboards/13105 dashboard
* Check https://helm.sh/docs/topics/provenance/
